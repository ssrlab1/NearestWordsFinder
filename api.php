<?php
	header("Content-type: text/html;  charset=utf-8");
	header("Access-Control-Allow-Origin: *");
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8');
		
	$localization = isset($_POST['localization']) ? $_POST['localization'] : 'en';
	$text = isset($_POST['text']) ? $_POST['text'] : '';
	$dictionary = isset($_POST['dictionary']) ? $_POST['dictionary'] : '';
	$knownWordsIgnore = isset($_POST['knownWordsIgnore']) ? $_POST['knownWordsIgnore'] : 0;
	
	include_once 'NearestWordsFinder.php';
	NearestWordsFinder::loadLocalization($localization);
	
	$msg = '';
	if(!empty($text)) {
		$NearestWordsFinder = new NearestWordsFinder($knownWordsIgnore);
		$NearestWordsFinder->setText($text);
		$NearestWordsFinder->setDictionary($dictionary);
		$NearestWordsFinder->run();
		$NearestWordsFinder->saveCacheFiles();

		$result['text'] = $text;
		$result['resultArr'] = $NearestWordsFinder->getResultArr();
		$msg = json_encode($result);
	}
	echo $msg;
?>