<?php
	header("Content-Type: text/html; charset=utf-8");
	$ini = parse_ini_file('service.ini');
	include_once 'NearestWordsFinder.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	$languages = NearestWordsFinder::loadLanguages();
	NearestWordsFinder::loadLocalization($lang);
?>
<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
	<head>
		<title><?php echo NearestWordsFinder::showMessage('title'); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href='css/theme.css'>
		<link rel="icon" type="image/x-icon" href="img/favicon.ico">
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
		<?php include_once 'analyticstracking.php'; ?>
		<script>
			var inputTextDefault = "<?php echo str_replace("\"", '\"', str_replace("\n", '\\n', NearestWordsFinder::showMessage('default input'))); ?>";
			var dictionaryDefault = "<?php echo str_replace("\"", '\"', str_replace("\n", '\\n', NearestWordsFinder::showMessage('default dictionary'))); ?>";
			$(document).ready(function () {
				$(document).on('click', 'button#MainButtonId', function() {
					$('#resultBlockId').show('slow');
					$('#resultId').empty();
					$('#resultId').prepend($('<img>', { src: "img/loading.gif"}));
					var knownWordsIgnore = $('input:checkbox[name=knownWordsIgnore]').prop('checked') == true ? 1 : 0;
					$.ajax({
						type: 'POST',
						url: 'https://corpus.by/NearestWordsFinder/api.php',
						data: {
							'localization': '<?php echo $lang; ?>',
							'text': $('textarea#inputTextId').val(),
							'dictionary': $('textarea#dictionaryId').val(),
							'knownWordsIgnore': knownWordsIgnore
						},
						success: function(msg) {
							var result = jQuery.parseJSON(msg);
							var output = '';
							if(result.resultArr) {
								output += '<table id="resultTableId" class="table table-sm table-striped" style="table-layout: fixed; width: 100%;">';
								output += '<thead><tr><th scope="col"><b>Unknown word</b></th><th scope="col"><b>Possible variant</b></th></tr></thead><tbody>';
								output += Object.keys(result.resultArr).reduce(
									function(previousValue, currentValue) {
										return(previousValue + '<tr><td style="word-wrap: break-word">' + currentValue
											+ '</td><td style="word-wrap: break-word">' + result.resultArr[currentValue] + '</td></tr>'
										);
									},
									"",
								);
								output += '</tbody></table>';
							}
							$('#resultId').html(output);
						},
						error: function() {
							$('#resultId').html('ERROR');
						}
					});
				});
			});
		</script>
	</head>
	<body>
		<!-- Novigation -->
		<nav class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/"><img style="max-width:150px; margin-top: -7px;" src="img/main-logo.png" alt=""></a>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li class="service-name"><a href="/NearestWordsFinder/?lang=<?php echo $lang; ?>"><?php echo NearestWordsFinder::showMessage('title'); ?></a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="service-name"><a href="<?php echo NearestWordsFinder::showMessage('help'); ?>" target="_blank">?</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo NearestWordsFinder::showMessage($lang); ?><span class="caret"></span></a>
							<ul class="dropdown-menu">
								<?php
									$languages = NearestWordsFinder::loadLanguages();
									foreach($languages as $language) {
										echo "<li><a href='?lang=$language'>" . NearestWordsFinder::showMessage($language) . "</a></li>";
									}
								?>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- End of Novigation -->
		<div class="container theme-showcase" role="main">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-8">
						<div class="control-panel">
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="btn-group pull-right">
										<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('inputTextId').value=inputTextDefault;"><?php echo NearestWordsFinder::showMessage('refresh'); ?></button>
										<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('inputTextId').value='';"><?php echo NearestWordsFinder::showMessage('clear'); ?></button>
									</div>
									<h3 class="panel-title"><?php echo NearestWordsFinder::showMessage('input'); ?></h3>
								</div>
								<div class="panel-body">
									<p><textarea class="form-control" rows="10" id = "inputTextId" name="inputText" placeholder="Enter text"><?php echo str_replace('\n', "\n", NearestWordsFinder::showMessage('default input')); ?></textarea></p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="control-panel">
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="btn-group pull-right">
										<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('dictionaryId').value=dictionaryDefault;"><?php echo NearestWordsFinder::showMessage('refresh'); ?></button>
										<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('dictionaryId').value='';"><?php echo NearestWordsFinder::showMessage('clear'); ?></button>
									</div>
									<h3 class="panel-title"><?php echo NearestWordsFinder::showMessage('dictionary'); ?></h3>
								</div>
								<div class="panel-body">
									<p><textarea class="form-control" rows="10" id = "dictionaryId" name="dictionary" placeholder="Enter text"><?php echo str_replace('\n', "\n", NearestWordsFinder::showMessage('default dictionary')); ?></textarea></p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="control-panel">
						<input type="checkbox" name="knownWordsIgnore" value="1" checked>&nbsp;<?php echo NearestWordsFinder::showMessage('known words ignore'); ?></input>
					</div>
				</div>
				<div class="col-md-12">
					<button type="button" id="MainButtonId" name="MainButton" class="button-primary"><?php echo NearestWordsFinder::showMessage('button'); ?></button>
				</div>
				<div class="col-md-12" id="resultBlockId" style="display: none;">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title"><?php echo NearestWordsFinder::showMessage('result'); ?></h3>
							</div>
							<div class="panel-body">
								<p id="resultId"></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-body">
								<?php echo NearestWordsFinder::showMessage('service code'); ?>&nbsp;<a href="https://gitlab.com/ssrlab1/NearestWordsFinder" target="_blank"><?php echo NearestWordsFinder::showMessage('reference'); ?></a>.
								<br />
								<a href="https://gitlab.com/ssrlab1/NearestWordsFinder/-/issues/new" target="_blank"><?php echo NearestWordsFinder::showMessage('suggestions'); ?></a>.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer class="footer">
			<div class="container">
				<p class="text-muted">
					<?php echo NearestWordsFinder::showMessage('contact e-mail'); ?>
					<a href="mailto:corpus.by@gmail.com">corpus.by@gmail.com</a>.<br />
					<?php echo NearestWordsFinder::showMessage('other prototypes'); ?>
					<a href="https://corpus.by/?lang=<?php echo $localizationLang; ?>">corpus.by</a>,&nbsp;<a href="https://ssrlab.by">ssrlab.by</a>.
				</p>
				<p class="text-muted">
					<?php echo NearestWordsFinder::showMessage('laboratory'), ', ', $ini['year']; if($ini['year'] !== date('Y')) echo '—', date('Y'); ?>
				</p>
			</div>
		</footer>
	</body>
</html>
<?php NearestWordsFinder::sendErrorList($lang); ?>