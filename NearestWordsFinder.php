<?php
	class NearestWordsFinder {
		private static $localizationArr = array();
		private static $localizationErr = array();
		private $text = '';
		private $dictionary = '';
		private $knownWordsIgnore = '';
		private $resultArr = array();
		private $apostrophesArr = array("'", "ʼ", "’", "‘");
		private $acuteAccentsArr = array("´", "\xcc\x81");
		private $graveAccent = "\xcc\x80";
		private $letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдежзийклмнопрстуфхцчшщъыьэюяЂЃѓЉЊЌЋЏђљњќћџЎўЈҐЁЄЇІіґёєјЅѕї';
		const BR = "<br>\n";

		function __construct($knownWordsIgnore) {
			$this->knownWordsIgnore = $knownWordsIgnore;
		}

		public function setText($text) {
			$this->text = $text;
		}

		public function setDictionary($dictionary) {
			$dictionary = mb_strtolower($dictionary, 'UTF-8');
			$this->dictionary = preg_split("/\s+/", $dictionary);
		}
		
		public static function loadLanguages() {
			$languages = array();
			$files = scandir('lang/');
			if(!empty($files)) {
				foreach($files as $file) {
					if(substr($file, 2, 4) == '.txt') {
						$languages[] = substr($file, 0, 2);
					}
				}
			}
			return $languages;
		}
		
		public static function loadLocalization($lang) {
			$filepath = "lang/$lang.txt";
			$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			if($linesArr === false) {
				$filepath = "lang/en.txt";
				$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			}
			foreach($linesArr as $line) {
				if(empty($line)) {
					$key = $value = '';
				}
				elseif(substr($line, 0, 1) !== '#' && substr($line, 0, 2) !== '//') {
					if(empty($key)) {
						$key = $line;
					}
					else {
						if(!isset(self::$localizationArr[$key])) {
							self::$localizationArr[$key] = $line;
						}
					}
				}
			}
		}
		
		public static function showMessage($msg) {
			if(isset(self::$localizationArr[$msg])) {
				return self::$localizationArr[$msg];
			}
			else {
				self::$localizationErr[] = $msg;
				return $msg;
			}
		}
		
		public static function sendErrorList($lang) {
			if(!empty(self::$localizationErr)) {
				$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
				$sendersName = 'Nearest Words Finder';
				$recipient = 'corpus.by@gmail.com';
				$subject = "ERROR: Incorrect localization in $sendersName by user $ip";
				$mailBody = 'Вітаю, гэта corpus.by!' . self::BR;
				$mailBody .= $_SERVER['HTTP_HOST'] . '/NearestWordsFinder/ дасылае інфармацыю аб наступных памылках:' . self::BR . self::BR;
				$mailBody .= "Мова лакалізацыі: <b>$lang</b>" . self::BR;
				$mailBody .= 'Адсутнічае лакалізацыя наступных ідэнтыфікатараў:' . self::BR . implode(self::BR, self::$localizationErr) . self::BR;
				$header  = "MIME-Version: 1.0\r\n";
				$header .= "Content-type: text/html; charset=utf-8\r\n";
				$header .= "From: $sendersName <corpus.by@gmail.com>\r\n";
				mail($recipient, $subject, $mailBody, $header);
			}
		}
		
		private function defineWords($string, $characterGroup1, $characterGroup2) {
			$wordsArr = array();
			if(!empty($characterGroup1)) {
				$pattern = "/(^|[$characterGroup1][$characterGroup1$characterGroup2]*)([^$characterGroup1]*)/u";
				preg_match_all($pattern, $string, $wordsArr, PREG_SET_ORDER);
			}
			return $wordsArr;
		}
		
		function levdistance($first, $second) {
			if ($first == $second) {
				return 0;
			}
			list($flen, $slen) = [strlen($first), strlen($second)];
			if ($flen == 0 || $slen == 0) {
				return $slen ? $slen : $flen;
			}
			$dist = range(0, $slen);
			for ($i = 0; $i < $flen; $i++) {
				$_dist = [$i + 1];
				for ($j = 0; $j < $slen; $j++) {
					$cost = ($first[$i] == $second[$j]) ? 0 : 1;
					$_dist[$j + 1] = min(
						$dist[$j + 1] + 1,
						$_dist[$j] + 1, 
						$dist[$j] + $cost 
					);
				}
				$dist = $_dist;
			}
			return $dist[$slen];
		}
		
		public function run() {
			mb_internal_encoding('UTF-8');
			$mainChars = $this->letters;
			$additionalChars = implode('', $this->acuteAccentsArr) . implode('', $this->apostrophesArr) . $this->graveAccent . '\-';
			$this->paragraphsArr = explode("\n", $this->text);
			foreach($this->paragraphsArr as $paragraph) {
				$paragraph = trim($paragraph);
				if(!empty($paragraph)) {
					$wordsArr = $this->defineWords($paragraph, $mainChars, $additionalChars);
					for($i=0; $i<count($wordsArr); $i++) {
						$word = isset($wordsArr[$i][1]) ? $wordsArr[$i][1] : '';
						$delimiter = isset($wordsArr[$i][2]) ? $wordsArr[$i][2] : '';
						$nearest = '';
						$distance = 999999;
						if(!empty($word) && !isset($this->resultArr[$word])) {
							foreach($this->dictionary as $entry) {
								$lev = $this->levdistance($word, $entry);
								if($lev == 0) {
									$nearest = $word;
									$distance = 0;
									break;
								}
								elseif($lev < $distance) {
									$nearest = $entry;
									$distance = $lev;
								}
							}
							if($this->knownWordsIgnore) {
								if($distance != 0) {
									$this->resultArr[$word] = $nearest;
								}
							}
							else {
								$this->resultArr[$word] = $nearest;
							}
						}
					}
				}
			}
			if(!empty($this->resultArr)) {
				
			}
		}
		
		public function saveCacheFiles() {
			mb_internal_encoding('UTF-8');
			mb_regex_encoding('UTF-8');
			
			$dateCode = date('Y-m-d_H-i-s', time());
			$randCode = rand(0, 1000);
			$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
			
			$root = $_SERVER['HTTP_HOST'];
			if($root == 'corpus.by') $root = 'https://corpus.by';
			$serviceName = 'NearestWordsFinder';
			$sendersName = 'Nearest Words Finder';
			$recipient = 'corpus.by@gmail.com';
			$subject = "$sendersName from IP $ip";
			$mailBody = 'Вітаю, гэта corpus.by!' . self::BR;
			$mailBody .= "$root/$serviceName/ дасылае інфармацыю аб актыўнасці карыстальніка з IP $ip." . self::BR . self::BR;
			$textLength = mb_strlen($this->text);
			$pages = round($textLength/2300, 1);
			
			$cachePath = dirname(dirname(__FILE__)) . "/_cache";
			if(!file_exists($cachePath)) mkdir($cachePath);
			$cachePath = "$cachePath/$serviceName";
			if(!file_exists($cachePath)) mkdir($cachePath);
			
			$filename = $dateCode . '_'. $ip . '_' . $randCode . '_in.txt';
			$path = "$cachePath/in/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			$cacheText = preg_replace("/(^\s+)|(\s+$)/us", '', $this->text);
			fwrite($newFile, $cacheText);
			fclose($newFile);
			$url = $root . "/showCache.php?s=$serviceName&t=in&f=$filename";
			if(mb_strlen($cacheText)) {
				$mailBody .= "Тэкст ($textLength сімв., прыкладна $pages ст. па 2300 сімв. на старонку) пачынаецца з:" . self::BR;
				preg_match('/([^\n]*\n?){1,3}/u', $cacheText, $matches);
				$str = str_replace("\n", self::BR, trim($matches[0]));
				if(mb_strlen($str) < 300) {
					$mailBody .= '<blockquote><i>' . $str . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
				}
				else {
					$mailBody .= '<blockquote><i>' . mb_substr($str, 0, 300) . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
				}
			}
			$mailBody .= self::BR;
			
			$filename = $dateCode . '_'. $ip . '_' . $randCode . '_out.txt';
			$path = "$cachePath/out/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			$cacheText = preg_replace("/(^\s+)|(\s+$)/us", "", htmlspecialchars(json_encode($this->resultArr, JSON_UNESCAPED_UNICODE)));
			fwrite($newFile, $cacheText);
			fclose($newFile);
			$url = $root . "/showCache.php?s=$serviceName&t=out&f=$filename";
			if(mb_strlen($cacheText)) {
				$mailBody .= "Вынік пачынаецца з:" . self::BR;
				if(mb_strlen($cacheText) < 1000) {
					$mailBody .= '<blockquote><i>' . $cacheText . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
				}
				else {
					$mailBody .= '<blockquote><i>' . mb_substr($cacheText, 0, 1000) . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
				}
			}
			$mailBody .= self::BR;
			
			$header  = "MIME-Version: 1.0\r\n";
			$header .= "Content-type: text/html; charset=utf-8\r\n";
			$header .= "From: $sendersName <corpus.by@gmail.com>\r\n";
			mail($recipient, $subject, $mailBody, $header);
			
			$filename = $dateCode . '_'. $ip . '_' . $randCode . '_e.txt';
			$path = "$cachePath/email/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			fwrite($newFile, join("\n", array("recipient: \t" . $recipient, "subject: \t" . $subject, "\n\tMAIL BODY\n\n" . $mailBody, $header)));
			fclose($newFile);
		}
		
		public function getResultArr() {
			return $this->resultArr;
		}
	}
?>